import React, { Component } from 'react';
import CloseIcon from '../../assets/closeIcon.png';
import styles from './styles.css';

class AgendaModal extends Component {
  constructor(props) {
    super();
    this.state = {
      name: '',
      schedule: {
        day: 'Senin',
        start: '',
        end: '',
        room: '',
      },
    };
    this.handleClickDate = this.handleClickDate.bind(this)
  }

  agendaChangeInput(field, value) {
    const currentState = this.state;
    let newState = currentState;

    if (Array.isArray(field)) {
      newState[field[0]][field[1]] = value;
    } else {
      newState[field] = value;
    }

    this.setState({
      ...newState,
    });
  }

  render() {
    const { name, schedule } = this.state;
    const { day, start, end, room } = schedule;
    console.log(this.state)
    return (
      <div className="modal-overlay">
        <div className="modal">
          <div className="close-button">
            <img src={CloseIcon} />
          </div>
          <div className="container">
            <div className="title">
              Tambah Agenda
            </div>
            <div className="form">
              <div className="left-form">
                <div className="input-form">
                  <div className="agenda-title">Nama Agenda</div>
                  <input value={name} onChange={(evt) => this.agendaChangeInput('name', evt.target.value)} placeholder="Rapat Compfest X" />
                </div>
                <div>
                  <div className="input-form">
                    <div className="agenda-title">Jam Mulai</div>
                    <input value={start} onChange={(evt) => this.agendaChangeInput(['schedule', 'start'], evt.target.value)} placeholder="format HH.MM" />
                  </div>
                </div>
              </div>
              <div className="right-form">
                <div>
                  <div className="input-form">
                    <div className="agenda-title">Lokasi</div>
                    <input value={room} onChange={(evt) => this.agendaChangeInput(['schedule', 'room'], evt.target.value)} placeholder="Belyos" />
                  </div>
                </div>
                <div>
                  <div className="input-form">
                    <div className="agenda-title">Jam Selesai</div>
                    <input value={end} onChange={(evt) => this.agendaChangeInput(['schedule', 'end'], evt.target.value)} placeholder="format HH.MM" />
                  </div>
                </div>
              </div>
            </div>
            <div className="date">
              <div>
                Hari
              </div>
              <div className="button-array">
                <button className={day === 'Senin' ? 'active' : ''} onClick={() => this.agendaChangeInput(['schedule', 'day'], 'Senin')}>Senin</button>
                <button className={day === 'Selasa' ? 'active' : ''} onClick={() => this.agendaChangeInput(['schedule', 'day'], 'Selasa')}>Selasa</button>
                <button className={day === 'Rabu' ? 'active' : ''} onClick={() => this.agendaChangeInput(['schedule', 'day'], 'Rabu')}>Rabu</button>
                <button className={day === 'Kamis' ? 'active' : ''} onClick={() => this.agendaChangeInput(['schedule', 'day'], 'Kamis')}>Kamis</button>
                <button className={day === 'Jumat' ? 'active' : ''} onClick={() => this.agendaChangeInput(['schedule', 'day'], 'Jumat')}>Jumat</button>
                <button className={day === 'Sabtu' ? 'active' : ''} onClick={() => this.agendaChangeInput(['schedule', 'day'], 'Sabtu')}>Sabtu</button>
              </div>
            </div>
            <div className="submit-button">
              <button onClick={this.handleClickSubmit}>Tambah</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AgendaModal;

import React, { Component } from 'react';
import logo from './logo.svg';
import SimpleModalLauncher from './components/SimpleModalLauncher';
import AgendaModal from './components/AgendaModal';

class App extends Component {
  render() {
    return (
        <AgendaModal />
    );
  }
}

export default App;
